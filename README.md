
To build this project run
```
  nix-shell
  cabal update
  cabal run
```
The program should abort after a couple of runs. I find that it happens approx 50% of the time, but
it might vary on a different machine.
