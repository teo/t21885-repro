let
  pkgs = import (builtins.fetchTarball
  "https://github.com/NixOS/nixpkgs/archive/e4d49de45a3b5dbcb881656b4e3986e666141ea9.tar.gz") {};
in pkgs.mkShell {
  buildInputs = [
    pkgs.haskell.compiler.ghc923
    pkgs.cabal-install
    pkgs.ncurses
    pkgs.wget
    pkgs.curl
    pkgs.zlib
    pkgs.elfutils
    pkgs.git
    pkgs.gmp
  ];
}
