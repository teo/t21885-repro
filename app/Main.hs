{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import Control.Concurrent         (forkIO, myThreadId, threadDelay)
import Control.Concurrent.MVar
import Control.Concurrent.STM
import Data.IORef
import Data.Conduit               (ConduitT, ConduitM, await, awaitForever, runConduit, (.|))
import Debug.Trace                
import GHC.Conc                   (labelThread)
import System.Mem                 
import Control.Parallel.Strategies (rdeepseq, usingIO)
import Data.Aeson (decodeStrict, Value)
import System.IO
import System.Exit
import Control.Monad.IO.Class
import Control.Monad
import Control.Exception
import Control.Concurrent.Async
import System.Random
import Data.Traversable
import Data.List (intercalate)

import qualified Control.Concurrent.STM          as STM
import qualified Data.Conduit.Combinators        as CC
import qualified Data.Vector                     as V
import qualified Data.ByteString.Char8 as B

go2
  :: (MonadIO m)
  => (ConduitT () () m () -> IO z)
  -> IO z
go2 go = do
  (writeMsg, readMsg) <- parse

  let notifyOfNewMessage (message, _) = do
        liftIO $ traceEventIO $ "notify"
        writeMsg message

      clientThread = clientThreadGo
      clientThreadGo = do 
       -- noKeys <- randomRIO (500, 2000)
       let noKeys = 1000
       pairs <- for [1..noKeys] $ \(k :: Int) -> do
         let size = 10
         str <- replicateM size (randomRIO ('A', 'Z'))
         pure $ "\"" <> show k <> "\":\"" <> str <>"\""
       let out = B.pack ("{" <> intercalate "," pairs <> "}")
       evaluate out
       notifyOfNewMessage (out, B.length out)
       clientThreadGo

  withAsync clientThread $ \_ -> 
    go (CC.replicateM 100 (liftIO $ traceEventIO "readMsg" >> readMsg) .| applyConduit)

applyConduit
  :: (Monad m, MonadIO m)
  => ConduitM ((Maybe Value)) () m ()
applyConduit = go
  where
  go = await >>= \case
    Nothing -> do
      liftIO $ traceEventIO "unexpected exit"
    Just aed -> do
      void $ liftIO $ usingIO aed rdeepseq
      liftIO performMajorGC
      liftIO $ traceEventIO "rec"
      go 

parse :: IO (B.ByteString -> IO (), IO (Maybe Value))
parse = mapMPara "parsePara" 24 $ f
  where
  f (rawEventContent) = do
    liftIO $ traceEventIO $ "time"
    let eventContent = decodeStrict rawEventContent
    threadDelay 10
    void $ usingIO eventContent rdeepseq
    case eventContent of
      Nothing -> traceM ("could not decode json" <> show rawEventContent) >> pure Nothing
      Just ied -> pure $ Just ied

mapMPara :: String -> Int -> (a -> IO b) -> IO (a -> IO (), IO b)
mapMPara label width f = do
  (ins, outs) <- liftIO $ do
    ins <- V.replicateM width newEmptyMVar
    outs <- V.replicateM width newEmptyMVar
    let
      setupThread n = do
        mtid <- myThreadId
        labelThread mtid (label ++ show n)
        mIn <- evaluate $ V.unsafeIndex ins n
        mOut <- evaluate $ V.unsafeIndex outs n
        forever ((takeMVar mIn) >>= \x -> f x >>= putMVar mOut)
    mapM_ (void . forkIO . setupThread) [0..width-1] 
    pure (ins, outs)
  writeCounter <- newIORef 0
  readCounter <- newIORef 0
  let inc arr ref = atomicModifyIORef' ref (\x -> ((x+1) `mod` width, V.unsafeIndex arr x))
  let write newVal =  do
        y <- inc ins writeCounter
        liftIO $ putMVar y newVal
  let read = do
        y <- inc outs readCounter
        liftIO $ takeMVar y
  pure (write, read)


main :: IO ()
main = do
  setStdGen $ mkStdGen 1
  let 
    streamAndApply source = source .| awaitForever (const $ liftIO $ threadDelay 200)
  go2 $ runConduit . streamAndApply
  exitSuccess
